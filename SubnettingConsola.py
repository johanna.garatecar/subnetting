import pandas as pd
import csv

def isPrimo(numero):
    count = 0

def calcular_redes(bits):
    global saltos

    for i in range(bits):
        if 2 ** i >= sr:
            n = i
            break
    m = bits - n
    saltos = 2 ** m

    print("bits: ", bits)
    #print("n: ", n)
    #print("m: ", m)
    print("Cantidad de host por subred: ", (2**m)-2)
    print("saltos:", saltos)
    print("\n")

    redes_decimal = []

    for i in range(sr):
        redes_decimal.append(agregar_0(bits, decimal_a_binario(i * saltos)))

    return redes_decimal


def decimal_a_binario(decimal):
    if decimal <= 0:
        return "0"
    binario = ""
    while decimal > 0:
        residuo = int(decimal % 2)
        decimal = int(decimal / 2)
        binario = str(residuo) + binario
    return binario


def binario_a_decimal(numero_binario):
    numero_decimal = 0

    for posicion, digito_string in enumerate(numero_binario[::-1]):
        numero_decimal += int(digito_string) * 2 ** posicion

    return numero_decimal

def last_red(oc1, oc2, oc3, oc4, salto):

    if(int(salto)<=255):
        r= (int(oc4)+(salto-1))

        first= str(oc1+"."+oc2+"."+oc3+ "."+ str(int(oc4)+1))
        last= str(oc1+"."+oc2+"."+oc3+ "."+ str(int(r)-1))
        broad= str(oc1+"."+oc2+"."+oc3+ "."+ str(r))

        return first+ " || "+ last + " || "+broad

    elif (int(salto)>=256 and int(salto)<=65535):
        t= round(float(salto/255))-1
        t= int(oc3)+int(t)

        first = str(oc1 + "." + oc2 + "." + oc3 + "." + str(int(oc4) + 1))
        last = str(oc1 + "." + oc2 + "." + str(t) + "." + str(int(255) - 1))
        broad= str(oc1+"."+oc2+"."+str(t)+ "."+str("255"))

        return first+ " || "+ last + " || "+broad

    else:
        t = round(round(float(salto / 255)) / 255)-1
        t = int(oc2) + int(t)-1

        first = str(oc1 + "." + oc2 + "." + oc3 + "." + str(int(oc4) + 1))
        last = str(oc1 + "." + str(t) + "." + str("255") + "." + str(int(255) - 1))
        broad= str(oc1 + "." + str(t) + "." + str("255") + "." + str("255"))

        return first+ " || "+ last + " || "+broad

    return ""

def agregar_0(bits, binario):
    while len(binario) < bits:
        binario = "0" + binario
    return binario

print("Bienvenida Johanna")
ip = input("Digite la ip de la red: ")
sr = int(input("Digite el numero de sub redes: "))

octales = ip.split(".")

redes_final = []

if int(octales[0]) <= 127:
    tipo = 'A'
    mask= '255.0.0.0'
    bits = 24
    redes_decimal = calcular_redes(bits)

    redes_final.append("DIREC. RED || PRIMER IP UTIL|| ULTIMA IP UTIL ||  BROADCAST")

    for i in redes_decimal:
        octal1 = i[0:8]
        octal2 = i[8:16]
        octal3 = i[16:24]



        redes_final.append(
            octales[0] + "." + str(binario_a_decimal(octal1)) + "." + str(binario_a_decimal(octal2)) + "." + str(
                binario_a_decimal(octal3)) + " || "+ str( last_red(octales[0], str(binario_a_decimal(octal1)), str(binario_a_decimal(octal2)), str(
                binario_a_decimal(octal3)), saltos) ))

elif int(octales[0]) <= 191:
    tipo = 'B'
    mask = '255.255.0.0'
    bits = 16
    redes_decimal = calcular_redes(bits)

    redes_final.append("DIREC. RED || PRIMER IP UTIL|| ULTIMA IP UTIL ||  BROADCAST")

    for i in redes_decimal:
        octal1 = i[:len(i) // 2]
        octal2 = i[len(i) // 2:]

        redes_final.append(
            octales[0] + "." + octales[1] + "." + str(binario_a_decimal(octal1)) + "." + str(binario_a_decimal(octal2))
                                + " || "+ str( last_red(octales[0], octales[1], str(binario_a_decimal(octal1)), str(binario_a_decimal(octal2)), saltos) ))


elif int(octales[0]) <= 223:
    tipo = 'C'
    mask = '255.255.255.0'
    bits = 8
    redes_decimal = calcular_redes(bits)

    redes_final.append("DIREC. RED || PRIMER IP UTIL|| ULTIMA IP UTIL ||  BROADCAST")

    for i in redes_decimal:
        redes_final.append(octales[0] + "." + octales[1] + "." + octales[2] + "." + str(binario_a_decimal(i))
                           + " || "+ str( last_red(octales[0], octales[1], octales[2], str(binario_a_decimal(i)), saltos) ))


#index = range(1, len(redes_final) + 1)
#df = pd.DataFrame(redes_final, index=index, columns=['SubRedes'])

for x in redes_final:
    print(x)

print(f"La red es de tipo {tipo}.")
print(f"La máscara de red es : {mask}")